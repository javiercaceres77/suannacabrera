 <?php

header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
session_start();

# control $_SESSION inactivity time
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
	// last request was more than 30 minates ago
	@session_destroy(); // destroy session data in storage
	session_unset(); // unset $_SESSION variable for the runtime
}

$_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
if($_GET['func'] == 'logout') session_unset();

# Includes ----------------------------------
include 'inc/config.php';
include $conf_include_path .'comm.php';
include $conf_include_path .'connect.php';
include $conf_include_path .'oops_comm.php';

if(!$_GET['lang'] && !$_SESSION['misc']['lang']) $_GET['lang'] = $conf_default_lang;
if($_GET['lang']) $_SESSION['misc']['lang'] = $_GET['lang'];
include $conf_include_path .'translation.php';
date_default_timezone_set($conf_timezone);

# Sanitize get and post ----------------------------------
sanitize_input();

# Logout user ----------------------------------
if($_GET['func'] == 'logout') {
	# store record of user logging out?
	session_unset(); session_destroy();
	jump_to($conf_main_page);
	exit();
}

# Get user info ----------------------------------
if(!isset($_SESSION['login']['user_id']))
	$_SESSION['login']['user_id'] = $conf_generic_user_id;
# Create objects for this page. ----------------------------------
if(!isset($ob_user))
	$ob_user = new user($_SESSION['login']['user_id'], $_SESSION['login']['name']);

$now = new date_time('now');
# Manage modules ----------------------------------
if(!$_GET['mod']) $_GET['mod'] = $conf_default_mod;
	refresh_users_modules(true);

?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="LCGaste Ltd" />
	<meta name="keywords" content="Sussana Cabrera moda complementos Santa Coloma ropa outfit" />
	<meta name="author" content="humans.txt">
    
	<link rel="icon" href="../../favicon.ico">

    <title>::: Susanna Cabrera :::</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="https://fonts.googleapis.com/css?family=Josefin+Slab|Stalemate" rel="stylesheet"> 
    <link href="css/main.css" rel="stylesheet">

	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="http://www.lcgaste.com">Lcgaste.com</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
		  <ul class="nav navbar-nav navbar-right">
			<!-- <li><a href="#myPage">INICIO</a></li> -->
			<li><a href="#quien">QUI SOM</a></li>
			<!-- <li><a href="#contact">HORARIO / CONTACTO</a></li> -->
			<li><a href="#donde">ON SOM</a></li>
			<!--    <li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">MORE
			<span class="caret"></span></a>
			<ul class="dropdown-menu">
			<li><a href="#">Merchandise</a></li>
			<li><a href="#">Extras</a></li>
			<li><a href="#">Media</a></li> 
			</ul>
			</li>
			<li><a href="#"><span class="glyphicon glyphicon-search"></span></a></li>-->
		  </ul>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>
    <div class="jumbotron" id="quien">
      <div class="container">
	    <h1>Susanna Cabrera</h1>
	    <h4>Moda i Complements</h4>
 	    <p>Susanna Cabrera &eacute;s el teu espai de moda, &eacute;s el projecte de Pilar Cabrera y Susanna Delgado.</p>
        <p>Fuig del producte massificat de les grans cadenes i descobreix petites col&middot;leccions vingudes d&rsquo;arreu d&rsquo;Europa que es renoven constantment per tal d&rsquo;oferir un producte seleccionat i diferent.
           <br>Col&middot;leccions per a dones modernes, independents, amb estil propi. Estigues a la &uacute;ltima! Deixa&rsquo;t assessorar </p>
      </div>
    </div>

	<div id="donde" class="container">
	  <h4 class="text-center">Contacte</h4>
	  <p class="text-center"><em>Localitzaci&oacute; i Horari</em></p>

	  <div class="row">
		<div class="col-md-4">
		  <p><span class="glyphicon glyphicon-map-marker"></span>Carrer Anselm Clavé
			 <br>08921 Sta Coloma de Gramanet</p>
		  <p><span class="glyphicon glyphicon-phone"></span>67474839282</p>
		  <p><span class="glyphicon glyphicon-envelope"></span>susannacabrera@gmail.com</p>
		</div>
		<div class="col-md-8">
		  <div class="row">
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1057.2593940809018!2d2.207862889939768!3d41.452647918455085!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4bc8dd4af74c5%3A0x2301f1983d01499!2sCarrer+Josep+Anselm+Clav%C3%A9%2C+08921+Santa+Coloma+de+Gramenet%2C+Barcelona%2C+Espa%C3%B1a!5e0!3m2!1ses!2suk!4v1541964237249" width="640" height="480" frameborder="0" style="border:0" allowfullscreen></iframe>
		  </div>
		</div>
	  </div>
	</div>
	
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
     
      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img src="ny.jpg" alt="New York" width="1200" height="700">
          <div class="carousel-caption">
            <h3>New York</h3>
            <p>The atmosphere in New York is lorem ipsum.</p>
          </div>      
        </div>
     
        <div class="item">
          <img src="chicago.jpg" alt="Chicago" width="1200" height="700">
          <div class="carousel-caption">
            <h3>Chicago</h3>
            <p>Thank you, Chicago - A night we won't forget.</p>
          </div>      
        </div>
      
        <div class="item">
          <img src="la.jpg" alt="Los Angeles" width="1200" height="700">
          <div class="carousel-caption">
            <h3>LA</h3>
            <p>Even though the traffic was a mess, we had the best time playing at Venice Beach!</p>
          </div>      
        </div>
      </div>
     
      <!-- Left and right controls -->
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>

	
    <footer>
      <p>&copy; Lcgaste ltd. <?= date('Y'); ?></p>
    </footer>
  </div> <!-- /container -->

	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="<?= $conf_include_path; ?>js/bootstrap.min.js"></script>
	<!--<script language="javascript" src="<?= $conf_include_path; ?>comm.js"></script>-->
	<!--<script language="javascript" src="<?= $conf_include_path; ?>ajax.js"></script>->
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
  </body>
</html>

